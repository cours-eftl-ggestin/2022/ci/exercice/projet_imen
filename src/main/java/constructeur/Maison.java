package constructeur;

public class Maison {

	private String materiau;
	
	private Integer surface;
	
	private BlocPorte blocPorte;
	
	public Maison(String materiau, Integer surface) {
		this.materiau = materiau;
		this.surface = surface;
	}
	
	public Maison(Integer surface) {
		this("brique", surface);
	}
	
	public Maison(String materiau) {
		this(materiau, 100);
	}
	
	public Maison() {
		this("brique", 100);
	}
	
	public Maison (String materiau, Integer surface, Integer hauteur, Integer largeur) {
		this(materiau, surface);
		this.blocPorte = new BlocPorte(hauteur, largeur);
	}
	
}

class BlocPorte {
	private Integer hauteur;
	private Integer largeur;
	
	public BlocPorte(Integer hauteur, Integer largeur) {
		this.hauteur = hauteur;
		this.largeur = largeur;
	}
}
