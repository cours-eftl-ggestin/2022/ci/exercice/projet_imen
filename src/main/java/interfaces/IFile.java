package interfaces;

public interface IFile {
	
	enum FileInfo {
		SEQUENTIAL,
		RELATIVE
	}
	
	static FileInfo getInfo(Boolean type) {
		return type?FileInfo.RELATIVE:FileInfo.SEQUENTIAL;
	}
}
