package interfaces;

public class ProgrammeFile {
	
	public static void main(String[] args) {
		
		File fichier = new File();
		fichier.write("MonFichier", "Contenu du fichier");
		
		System.out.println(fichier.read("MonFichier"));
		
		File2 fichier2 = new File2();
		fichier2.write("MonFichier2", "Contenu du fichier 2");
		System.out.println(fichier2.read("MonFichier2"));
	}

}
