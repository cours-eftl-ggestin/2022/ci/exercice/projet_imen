package interfaces;

public interface IWrite {
	public Boolean write (String filename, String content);
}
