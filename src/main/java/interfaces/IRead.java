package interfaces;

public interface IRead {

	public String read(String filename);
	
}
