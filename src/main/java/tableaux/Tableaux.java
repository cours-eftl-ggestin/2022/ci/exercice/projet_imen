package tableaux;

public class Tableaux {
	
	private Commande[] commandes = new Commande[4];
	
	/*private Commande[] commandes2 = { new Commande(10, "Client 1"),
										new Commande(20, "Client 2"),
										new Commande(30, "Client 3"),
										new Commande(40, "Client 4")};*/
	
	public void afficheTableau() {
		for (int indice = 0; indice < commandes.length; indice++) {
			commandes[indice].affiche();
		}
	}
	
	public void chargement() {
		commandes[0] = new Commande(1, "Client 1");
		commandes[1] = new Commande(2, "Client 2");
		commandes[2] = new Commande(3, "Client 3");
		commandes[3] = new Commande(4, "Client 4");
	}

}
