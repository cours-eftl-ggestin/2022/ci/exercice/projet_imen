package tableaux;

public class TableauPassage {

	private String[] tableau = { "Oyonnax", "Gex", "Belley", "Bourg en Bresse" };
	
	public void envoiTableau() {
		receptionTableau(this.tableau);
	}
	
	public void receptionTableau(String[] tableau) {
		for (String valeur : tableau) {
			System.out.println(valeur);
		}
	}
}
